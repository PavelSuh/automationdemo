import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class main {
    public static void main(String[] args) throws InterruptedException {
        String property = System.getProperty("user.dir") + "/drivers/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //переход на dneprbeta.itstep.org
        driver.get("http://dneprbeta.itstep.org");

        //закрываем рекламный поп-ап
        while (driver.findElement(By.xpath("//div[@class=\"demo-propose\"]//a[@id=\"demo_cancel\"]")).isDisplayed()==false){
            Thread.sleep(500);
        }
        driver.findElement(By.xpath("//div[@class=\"demo-propose\"]//a[@id=\"demo_cancel\"]")).click();

        //Переходим по первому елементу списка бокового меню
        driver.findElement(By.xpath("/html/body/header/div[3]/nav/ul[1]/li[1]/button")).click();
        while (driver.findElement(By.xpath("/html/body/header/div[3]/nav/ul[1]/li[1]/div/div[1]/ul/li[1]/a/span")).isDisplayed()==false){
            Thread.sleep(500);
        }
        driver.findElement(By.xpath("/html/body/header/div[3]/nav/ul[1]/li[1]/div/div[1]/ul/li[1]/a/span")).click();

        //Нажимаем на кнопку вызова формы
        while (driver.findElement(By.xpath("/html/body/main/section[1]/div/div/div[2]/div[2]/a")).isDisplayed()==false){
            Thread.sleep(500);
        }
        driver.findElement(By.xpath("/html/body/main/section[1]/div/div/div[2]/div[2]/a")).click();

        //Заполняем форму
        WebElement inputFIO = driver.findElement(By.xpath("//*[@id=\"form_construct_main\"]/div[2]/input"));
        inputFIO.sendKeys("test_pavel_auto");
        WebElement inputPhone = driver.findElement(By.xpath("//*[@id=\"form_construct_main\"]/div[3]/input"));
        inputPhone.sendKeys("981112233");
        WebElement inputEmail   = driver.findElement(By.xpath("//*[@id=\"form_construct_main\"]/div[4]/input"));
        inputEmail.sendKeys("test@pavel.auto");

        //Отправляем форму
        Thread.sleep(1000);
        WebElement submitButton = driver.findElement(By.xpath("//*[@id=\"form_construct_main\"]/button"));
        submitButton.click();
        Thread.sleep(1000);

        //переход на dev.crm.itstep.org
        driver.get("http://dev.crm.itstep.org");

        //Авторизация
        WebElement loginInput = driver.findElement(By.xpath("//*[@id=\"loginform-username\"]"));
        loginInput.sendKeys("rochev");
        WebElement passInput = driver.findElement(By.xpath("//*[@id=\"loginform-password\"]"));
        passInput.sendKeys("BGj527xs8IBbmR598Ryn");
        WebElement enterBtn = driver.findElement(By.xpath("//*[@id=\"enterBtn\"]"));
        enterBtn.click();

        //Переход на "Онлайн запросы"
        while (driver.findElement(By.xpath("//*[@id=\"online_req_icon\"]")).isDisplayed()==false){
            Thread.sleep(500);
        }
        driver.findElement(By.xpath("//*[@id=\"online_req_icon\"]")).click();

        //Находим нашу запись и удаляем
        Thread.sleep(3000);
        WebElement delete = driver.findElement(By.xpath("//td[@data-info=\"test_pavel_auto\"]/parent::tr//a[@class=\"delete_source delete_request_line\"]"));
        delete.click();
        while (driver.findElement(By.xpath("/html/body/div[6]/div/div/div[3]/button[1]")).isDisplayed()==false){
            Thread.sleep(500);
        }
        driver.findElement(By.xpath("/html/body/div[6]/div/div/div[3]/button[1]")).click();

        driver.quit();

    }
}

